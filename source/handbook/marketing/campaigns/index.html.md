---
layout: markdown_page
title: "Marketing Campaigns"
description: "GitLab Marketing Handbook: Campaigns"
twitter_image: '/images/tweets/handbook-marketing.png'
---

## On this page
{: .no_toc}

- TOC
{:toc .toc-list-icons}

---

## Integrated Campaigns

The Marketing department's effort is organized into Integrated Campaigns. An Integrated Campaign is a communication effort that includes several campaign tactics such as blog posts, emails, events, advertisements, content on about.gitlab.com, videos, case studies, whitepapers, surveys, social outreach, and webcasts. An Integrated Campaign will have a campaign theme that summarizes the message we are communicating to our market.

We track Integrated Campaign results in Salesforce and costs associated with Integrated Campaigns in Netsuite. In Salesforce, Integrated Campaigns are tracked as `parent campaigns` and campaign tactics are tracked as `campaigns`. A campaign tactic will have a `Type` and `Type Detail` to identify the category of effort for the purpose of identifying what types of marketing investment are most efficient in terms of the ratio between customer acquisition cost (CAC) and customer lifetime value (LTV).

### Active integrated campaigns

#### Just Commit

* [Parent epic with child epics and issues >>](https://gitlab.com/groups/gitlab-com/marketing/-/epics/7)
* [Meeting recordings >>](https://drive.google.com/drive/u/1/folders/147CtTEPz-fxa0m1bYxZUbOPBik-dkiYV)
* [Meeting slide deck >>](https://docs.google.com/presentation/d/1i2OzO13v77ACYo1g-_l3zV4NQ_46GX1z7CNWFsbEPrA/edit#slide=id.g153a2ed090_0_63)

##### 🎯 Target personas

**Level:** VP, Director, Lead, Head  
**Function:** Applications, Development

##### 🔑 Key Messages - 3 Pillars

##### P1: Modern application architecture

**Headlines**

* Just commit to software modernization.
* Need to modernize your software architecture? Just commit.

**Statements**

* Don't let your code get shelved by long deployment cycles. Pull revenue forward by getting code to production faster.
* Discover how to increase speed of innovation and improve productivity by ensuring your developers are focused on solving business problems, not deployment problems.


##### P2: Reduce cycle time

**Headlines**

* Just commit to reduce cycle times.
* Need to reduce cycle times? Just commit.

**Statements**

* Reduce software cycle time to deliver business value faster.
* Discover how to remove bottlenecks, align your team, and accelerate software delivery.

##### P3: Secure apps

**Headlines**

* Just commit to delivering secure apps.
* Need to release more secure applications? Just commit.

**Statements**

* Stay out of headlines by integrating security into your development process.
* Discover how to automate security testing, ensuring every bit of code is scanned before it leaves the developer’s hands.

##### Calendar

<iframe src="https://calendar.google.com/calendar/b/1/embed?showPrint=0&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=gitlab.com_co5678rr7jercc9lv11u0utng8%40group.calendar.google.com&amp;color=%23182C57&amp;ctz=America%2FSantiago" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>

#### Competitive campaign

* [Campaign Brief >>](https://docs.google.com/document/d/1xtyv59PFUSkVSqOGj0oGpLYAppjV0-nUZiddIwjQPYw/edit?usp=sharing)
* [Parent epic with child epics and issues >>](https://gitlab.com/groups/gitlab-com/marketing/-/epics/10)
* [SFDC Campaign >>](https://gitlab.my.salesforce.com/70161000000VxvJ)
* [Meeting recordings >>](https://drive.google.com/drive/folders/1q5foxAX_Ezy6FGdLxKMXLwCpHkwxIRAA?usp=sharing)
* [Meeting slide deck >>](https://docs.google.com/presentation/d/1eaMBQjXZ8v5R8KGEB1H_DZ4hPpML8heP5X6lq8a3Q_U/edit#slide=id.g2823c3f9ca_0_30)
* [Questions? Slack us at the campaign channel >>](https://gitlab.slack.com/messages/CGBH5M8A3/)

##### Calendar

<iframe src="https://calendar.google.com/calendar/embed?src=gitlab.com_viegf6bd2ebc9n8gnmm71gqcrs%40group.calendar.google.com&ctz=America%2FLos_Angeles" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>


## New Ideas for Marketing Campaigns or Events

- Ideas for marketing campaigns
   - We begin by asking: "Who is this for? What do they think? What do we want them to think?
   - If you have a good idea of who the campaign is for and why it would benefit them and us, create an issue in the marketing repo detailing the campaign concept, and tag the content and product marketing teams.
- Ideas for events
  - We are constantly on the look out for high value events, both large and small, to attend or sponsor, in order to engage with our target audiences.
  - Depending on the number and type of attendees at an event, it may be owned by Corporate Marketing, Field Marketing, or Community Relations. Please review our [events decision tree](https://docs.google.com/spreadsheets/d/1aWsmsksPfOlX1t6TeqPkh5EQXergt7qjHAjGTxU27as/edit?usp=sharing) to ensure you are directing your event inquiry to the appropriate team.
  - Direct your request to the appropriate team by creating an issue in the marketing repo and tagging someone from the proper team. For example: If your event qualifies as a Field Marketing event, create an issue in the marketing repo describing the event and rationale to attend/sponsor, and tag the field marketing manager.
- Both teams reserve the right to decline, but we love hearing your ideas! Understand that we need to fit every request into a program we’re running to meet our OKRs, as well as balance the needs of our community and our business.
- If it’s an item you can execute without much help, you’re more likely to be given a green light (e.g., a wallpaper that the design team can easily create).

Note: The Marketing Team owns content on marketing pages; do not change content or design of these pages without consulting the Manager, Digital Marketing Programs. Marketing will request help from product/UX when we need it, and work with them to ensure the timeline is reasonable.